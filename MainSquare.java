import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class MainSquare {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList points = new ArrayList();
        while (sc.hasNextLine()) {
            if (sc.hasNextDouble()) {
                Point point = new Point(sc.nextDouble(), sc.nextDouble());
                points.add(point);
            } else break;
        }
        SquareOfPolygon square = new SquareOfPolygon();
        DecimalFormat df = new DecimalFormat("#.00");

        System.out.println(df.format(square.square(points)).replaceAll(",","."));
    }
}
