import java.util.ArrayList;

public class SquareOfPolygon {

    public double square(ArrayList points){
        points.add(points.get(0));
        double sumXY=0;
        double sumYX=0;
        for (int i = 0; i < points.size()-1; i++) {
            Point firstPoint  = (Point) points.get(i);
            Point secondPoint = (Point) points.get(i+1);
            sumXY+=firstPoint.getX()*secondPoint.getY();
            sumYX+=firstPoint.getY()*secondPoint.getX();
        }
        return (sumXY-sumYX)/2;
    }


}